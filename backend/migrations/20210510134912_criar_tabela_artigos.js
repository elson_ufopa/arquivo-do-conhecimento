
exports.up = function(knex, Promise) {
  return knex.schema.createTable('artigos', table => {
    table.increments('nome').notNull()
    table.string('descricao', 1000).notNull()
    table.string('imageUrl', 1000)
    table.binary('content').notNull()
    table.integer('userId').references('id')
      .inTable('users').notNull()
    table.integer('categoriaId').references('id')
      .inTable('categorias').notNull()
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('artigos')
};
