
exports.up = function(knex, Promise) {
  return knex.schema.createTable('categorias', table => {
    table.increments('id').primary()
    table.string('nome').notNull()
    table.integer('parentId').references('id')
      .inTable('categorias')
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('categorias')
};
